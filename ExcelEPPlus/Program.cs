﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using ExcelEPPlus;
using ExcelToJsonConverter;
using Newtonsoft.Json;
using OfficeOpenXml;


namespace ExcelToJsonConverter
{

    class Program
    {
        static void Main(string[] args)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // Set the license context to NonCommercial
            string filePath = "C:\\Users\\lenovo\\Downloads\\SaaS.xlsx";

            List<ExcelData> excelData = ReadExcel(filePath);
            string outputFilePath = "C:\\Users\\lenovo\\Downloads\\output_epplus_excel.xlsx";
            WriteExcel(outputFilePath, excelData);

            Console.WriteLine("Data written to a new Excel file.");

            if (filePath == null)
            {
                Console.WriteLine("Upload A Excel File");
            }
            else
            {

                List<ExcelData> jsonList = ReadExcel(filePath);
                string json = JsonConvert.SerializeObject(jsonList);
                //Console.WriteLine(json);
                //List<ExcelData> deserializedData = JsonConvert.DeserializeObject<List<ExcelData>>(json);

                // Display the deserialized data

                foreach (var data in jsonList)
                {
                    Console.WriteLine($" Risk Category: {data.RiskCategory},\n Control Category: {data.ControlCategory},\n Question ToolTip: {data.QuestionToolTip},\n SaaS Assessment Questions: {data.SaaSAssessmentQuestions},\n Answer: {data.Answer},\n Comments: {data.Comments} \n");
                }

            }
        }

        
        static List<ExcelData> ReadExcel(string filePath)
        {

            List<ExcelData> excelDataList = new List<ExcelData>();

            FileInfo fileInfo = new FileInfo(filePath);

            using (ExcelPackage package = new ExcelPackage(fileInfo))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[2]; // Assuming data is in the third sheet

                int rowCount = worksheet.Dimension.Rows;

                for (int row = 2; row <= rowCount; row++) // Starting from row 2 as row 1 is header
                {
                    ExcelData excelData = new ExcelData();

                    excelData.RiskCategory = worksheet.Cells[row, 1].Value?.ToString() ?? string.Empty; // Assuming Risk Category is in column A (index 1)
                    excelData.ControlCategory = worksheet.Cells[row, 3].Value?.ToString() ?? string.Empty; // Assuming Control Category is in column B (index 2)
                    excelData.QuestionToolTip = worksheet.Cells[row, 4].Value?.ToString() ?? string.Empty;
                    excelData.SaaSAssessmentQuestions = worksheet.Cells[row, 5].Value?.ToString() ?? string.Empty;
                    excelData.Answer = worksheet.Cells[row, 6].Value?.ToString() ?? string.Empty;
                    excelData.Comments = worksheet.Cells[row, 7].Value?.ToString() ?? string.Empty;

                    excelDataList.Add(excelData);
                }
            }

            return excelDataList;
        }
        static void WriteExcel(string filePath, List<ExcelData> excelData)
        {
            FileInfo newFile = new FileInfo(filePath);

            using (ExcelPackage package = new ExcelPackage(newFile))
            {
                
                ExcelWorksheet worksheet1 = package.Workbook.Worksheets.Add("DataSheet1");
                

                // Header row
                worksheet1.Cells[1, 1].Value = "Risk Category";
                worksheet1.Cells[1, 2].Value = "Control Category";
                worksheet1.Cells[1, 3].Value = "Question Tooltip";
                worksheet1.Cells[1, 4].Value = "SaaS Assessment Questions";
                worksheet1.Cells[1, 5].Value = "Answer";
                worksheet1.Cells[1, 6].Value = "Comments";

                int row = 2;
                foreach (var data in excelData)
                {
                    worksheet1.Cells[row, 1].Value = data.RiskCategory;
                    worksheet1.Cells[row, 2].Value = data.ControlCategory;
                    worksheet1.Cells[row, 3].Value = data.QuestionToolTip;
                    worksheet1.Cells[row, 4].Value = data.SaaSAssessmentQuestions;
                    worksheet1.Cells[row, 5].Value = data.Answer;
                    worksheet1.Cells[row, 6].Value = data.Comments;

                    row++;
                }

                package.Save(); // Save the new Excel file
            }
        }
    }
}

