﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelEPPlus
{
    public class ExcelData
    {
        public string RiskCategory { get; set; }
        public string ControlCategory { get; set; }
        public string QuestionToolTip { get; set; }
        public string SaaSAssessmentQuestions { get; set; }
        public string Answer { get; set; }
        public string Comments { get; set; }
    }
}
